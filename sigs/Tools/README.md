
# Tools

openGauss工具生态的长期规划，发展和演进。


# 组织会议

- 公开的会议时间：北京时间，每双周四下午，16点30~17点30


# 成员


### Maintainer列表

- muting[@jeffee](https://gitee.com/jeffee)，*muting@huawei.com*


### Committer列表

- zhoubin[@justbk](https://gitee.com/justbk)，*249396768@qq.com*
- yuzhenglin[@popastin](https://gitee.com/popastin)，*93447842@qq.com*
- lijianfeng[@Andes_lee](https://gitee.com/Andes_lee)，*lijianfeng.lijianfeng@huawei.com*


# 联系方式

- [邮件列表](https://mailweb.opengauss.org/postorius/lists/tools.opengauss.org/)
- 视频会议


# 仓库清单

仓库地址：
- https://gitee.com/opengauss/openGauss-server

